[![pipeline status](https://gitlab.com/djieno/ttn_exporter_gps/badges/master/pipeline.svg)](https://gitlab.com/djieno/le_certificate_renewal/commits/master)

# Introduction

This is an exporter for retrieving GPS information from 'The Thing network' using their API. The GPS coordinates 
are received from a Lilygo gps module through LoRawan.

# Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [The Things Network api Documentation](https://www.thethingsnetwork.org/docs/applications/apis/)
- [Go promhttp](github.com/prometheus/client_golang/prometheus/promhttp")

# Getting started

First thing to do is add the API setting in the CICD values from The Things Network (TTN) which is used by `src/ttn.go`

## What's contained in this project

- src/ttn.go - is the main definition of the service, handler and client

## Run Service

```shell
go run src/ttn.go
```

## Query Service

```
curl http://localhost:9777/metrics
```

## Run docker Service

```
docker run -d --name=ttn -p 9777:9777 djieno/ttn
```