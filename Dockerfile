FROM golang:alpine AS builder

ENV APP_NAME tnn
ENV PORT 9777

WORKDIR $GOPATH/src/${APP_NAME}
COPY src/ttn.go .
RUN go mod init
RUN go mod tidy

# Build the binary.
RUN CGO_ENABLED=0 GOOS=linux go build -a -tags netgo -ldflags '-w' -o ${APP_NAME}

FROM alpine
COPY --from=builder go/src/${APP_NAME} ./

EXPOSE ${PORT}

CMD ["./ttn"]